FROM node:v18.16.0-alpine

RUN mkdir -p /opt/app
COPY package.json src/package-lock.json ./
RUN npm install
COPY src/ /opt/app

EXPOSE 8080
CMD [ "npm", "start" ]
