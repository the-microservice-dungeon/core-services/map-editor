//export type { World }

export type Planet = {
    id: number;
    x: number;
    y: number;
    movementDifficulty: number;
    resource: Resource | null;
  }
  
export type Resource = {
  resourceType: string;
  maxAmount: number;
  currentAmount: number;
}

export type World = {
  id: number;
  name: string;
  status: string;
  planets: Planet[];
}