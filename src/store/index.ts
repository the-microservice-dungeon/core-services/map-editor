import { createStore } from "vuex";
import type { World } from '@/components/mapfiletype' 

import axios from 'axios';

//adjust url to make calls to map service here for whole app!
const  url = 'http://localhost:8081/gameworlds'

const store = createStore({
    state() {
        return {
            gameData: null as World | null,
        }
        
    },
    mutations:{
        setGameData(state, data) {
            state.gameData = data as World;
        }
    },
    actions:{
        async getThisGameworld({commit}, id: number){
            try{
                const response = await axios.get(url+ "/" +id);
                commit('setGameData', response.data);
            }catch(error){
                console.error(error);
            }
        },
        async createGameworld({commit}, json: any): Promise<number | null>{
            try{
                const response = await axios({
                    method: 'post',
                    url: url ,
                    data: json
                })
                return response.data.gameworldId;
            }catch(error){
                console.error(error);
                return null;
            }
        },
        async saveThisGameworld(){
            if(store.state.gameData==null)
                return;
            try{
                const response = await axios({
                    method: 'post',
                    url: url+ "/" +store.state.gameData.id,
                    data: store.state.gameData
                    
                })
            }catch(error){
                console.error(error);
            }

        },
        async deleteThisGameworld({commit}, id: number){
            try{
                const response = await axios.delete(url+ "/" +id);
            }catch(error){
                console.error(error);
            }
        }
    }
})

export default store